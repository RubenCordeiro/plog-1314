:- use_module(library(clpfd)).
:- use_module(library(lists)).
:- use_module(library(sets)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% KB
%% generate([school_year]).
%%
%% school_year([subject], [class]).
%%
%% class(Number_Of_Students)
%%
%% subject(Name, (Number_T, Room_Type_T, Dur_T), (Number_TP, Room_Type_TP, Dur_TP), (Number_PL, Room_Type_PL, Dur_PL))
%%
%% teaches(Teacher, Subject_Name, Lesson_Type, [Class_Number])
%%
%% room(Identifier, Room_Type, Capacity)


%% room(Identifier, Room_Type, Capacity)
%% subject(Course, Year, Semester, Name, (Number_T, Room_Type_T, Dur_T), (Number_TP, Room_Type_TP, Dur_TP), (Number_PL, Room_Type_PL, Dur_PL)) 
%% class(Course, Year, Class_Number, Number_Students)
%% teaches(Teacher, Course, Subject_Name, Lesson_Type, [Class_Number])


room('B001', 'Amphitheater', 184).
room('B002', 'Amphitheater', 184).
room('B003', 'Amphitheater', 184).
room('B004', 'Amphitheater', 99).
room('B203', 'Informatics', 20).
room('B205', 'Informatics', 20).
room('B206', 'Informatics', 20).
room('B207', 'Informatics', 20).
room('B208', 'Informatics', 20).
room('B209', 'Informatics', 20).
room('B221', 'Normal', 60).
room('B222', 'Normal', 60).
room('B223', 'Normal', 60).
room('B224', 'Normal', 60).
room('B225', 'Normal', 60).
room('B226', 'Normal', 60).

lunch_break(9, 12, 3).

subject('MIEIC', 1, 'FPRO').
subject('MIEIC', 1, 'AMAT').
subject('MIEIC', 1, 'ALGE').
subject('MIEIC', 1, 'AOCO').
subject('MIEIC', 1, 'MDIS').

subject_lesson('MIEIC', 'FPRO', 'T', 2, 'Amphitheater', 3).
subject_lesson('MIEIC', 'FPRO', 'TP', 1, 'Informatics', 4).
subject_lesson('MIEIC', 'AMAT', 'T', 2, 'Amphitheater', 3).
subject_lesson('MIEIC', 'AMAT', 'TP', 1, 'Normal', 4).
subject_lesson('MIEIC', 'ALGE', 'T', 2, 'Amphitheater', 3).
subject_lesson('MIEIC', 'ALGE', 'TP', 1, 'Normal', 4).
subject_lesson('MIEIC', 'AOCO', 'T', 2, 'Amphitheater', 3).
subject_lesson('MIEIC', 'AOCO', 'TP', 1, 'Normal', 4).
subject_lesson('MIEIC', 'MDIS', 'T', 1, 'Amphitheater', 4).
subject_lesson('MIEIC', 'MDIS', 'TP', 1, 'Informatics', 4).

class('MIEIC', 1, 1, 20, morning).
class('MIEIC', 1, 2, 20, morning).
class('MIEIC', 1, 3, 20, morning).
class('MIEIC', 1, 4, 20, morning).
class('MIEIC', 1, 5, 20, morning).
class('MIEIC', 1, 6, 20, morning).

teaches('AFCC', 'MIEIC', 'FPRO', 'T', [1, 2, 3, 4, 5, 6]).

teaches('RCS',  'MIEIC', 'FPRO', 'TP', [1]).
teaches('JJ',   'MIEIC', 'FPRO', 'TP', [2]).
teaches('RCS',  'MIEIC', 'FPRO', 'TP', [3]).
teaches('JGB',  'MIEIC', 'FPRO', 'TP', [4]).
teaches('AFCC', 'MIEIC', 'FPRO', 'TP', [5]).
teaches('JJ',   'MIEIC', 'FPRO', 'TP', [6]).

teaches('AMF', 'MIEIC', 'ALGE', 'T', [1, 2, 3, 4, 5, 6]).

teaches('AMF',  'MIEIC', 'ALGE', 'TP', [1]).
teaches('AMF',  'MIEIC', 'ALGE', 'TP', [2]).
teaches('AMF',  'MIEIC', 'ALGE', 'TP', [3]).
teaches('CMCR', 'MIEIC', 'ALGE', 'TP', [4]).
teaches('AMF',  'MIEIC', 'ALGE', 'TP', [5]).
teaches('CMCR', 'MIEIC', 'ALGE', 'TP', [6]).

teaches('RMV', 'MIEIC', 'AOCO', 'T', [1, 2, 3, 4, 5, 6]).

teaches('JCF',   'MIEIC', 'AOCO', 'TP', [1]).
teaches('CMBNS', 'MIEIC', 'AOCO', 'TP', [2]).
teaches('AJA',   'MIEIC', 'AOCO', 'TP', [3]).
teaches('JCF',   'MIEIC', 'AOCO', 'TP', [4]).
teaches('JCF',   'MIEIC', 'AOCO', 'TP', [5]).
teaches('JCF',   'MIEIC', 'AOCO', 'TP', [6]).

teaches('AMF', 'MIEIC', 'AMAT', 'T', [1, 2, 3, 4, 5, 6]).

teaches('TCRD',    'MIEIC', 'AMAT', 'TP', [4]).
teaches('AMAN',    'MIEIC', 'AMAT', 'TP', [5]).
teaches('AMAN',    'MIEIC', 'AMAT', 'TP', [3]).
teaches('JaimeDR', 'MIEIC', 'AMAT', 'TP', [2]).
teaches('JaimeDR', 'MIEIC', 'AMAT', 'TP', [6]).
teaches('JaimeDR', 'MIEIC', 'AMAT', 'TP', [1]).

teaches('GTD', 'MIEIC', 'MDIS', 'T', [1, 2, 3, 4, 5, 6]).

teaches('EDMR',  'MIEIC', 'MDIS', 'TP', [1]).
teaches('EDMR',  'MIEIC', 'MDIS', 'TP', [2]).
teaches('GTD',   'MIEIC', 'MDIS', 'TP', [3]).
teaches('GTD',   'MIEIC', 'MDIS', 'TP', [4]).
teaches('EDMR',  'MIEIC', 'MDIS', 'TP', [5]).
teaches('CMC',   'MIEIC', 'MDIS', 'TP', [6]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

list_sum(List, Sum) :-
    list_sum(List, 0, Sum).

list_sum([], Accumulator, Accumulator).

list_sum([Head|Tail], Accumulator, Result) :-
    NewAccumulator is Accumulator + Head,
    list_sum(Tail, NewAccumulator, Result).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

maplist(Pred, Xs, Ys, Zs, Ws) :-
        (   foreach(X,Xs),
            foreach(Y,Ys),
            foreach(Z,Zs),
            foreach(W,Ws),
            param(Pred)
        do  call(Pred, X, Y, Z, W)
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

lse([],W1,W2,W1,W2,_):- !.

lse([H|T],L1,L2,W1,W2,l):- lse(T,[H|L1],L2,W1,W2,p), !.

lse([H|T],L1,L2,W1,W2,p):- lse(T,L1,[H|L2],W1,W2,l), !.

split([],[],[]).

split([H|T],[H|U],V):- split(T,V,U).

merge([H1|T1],[H2|T2],[H1|T], Pred):-
    call(Pred, H1, H2), !,
    merge(T1,[H2|T2],T, Pred).

merge([H1|T1],[H2|T2],[H2|T], Pred):-
     merge([H1|T1],T2,T, Pred),!.

merge(X,[],X, _):-!.

merge([],X,X, _).

mergesort([],[], _):- !.

mergesort([H],[H], _):- !.

mergesort(L,S, Pred):-
     split(L,LL,LR),
     mergesort(LL,SLL, Pred),
     mergesort(LR,SLR, Pred),
     merge(SLL,SLR,S, Pred).

less_than(I1, I2) :- I1 < I2.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

range(High, High, [High]) :- !.
range(Low, High, [Low | Tail]) :- Low < High, NewLow is Low + 1, range(NewLow, High, Tail).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

flatten(List, FlatList) :-
        flatten(List, [], FlatList0), !,
        FlatList = FlatList0.

flatten(Var, Tl, [Var|Tl]) :-
        var(Var), !.
flatten([], Tl, Tl) :- !.
flatten([Hd|Tl], Tail, List) :- !,
        flatten(Hd, FlatHeadTail, List),
        flatten(Tl, Tail, FlatHeadTail).
flatten(NonList, Tl, [NonList|Tl]).

%%%%%%%%%%%%%%%%%%%%%%
% SUBJECT PREDICATES %
%%%%%%%%%%%%%%%%%%%%%%

subject_year(Course, Subject, Year) :-
    subject(Course, Year, Subject).

%%%%%%%%%%%%%%%%%%%%%%
%  CLASS PREDICATES  %
%%%%%%%%%%%%%%%%%%%%%%

class_size(Course, Year, Class, Number_Students) :-
    class(Course, Year, Class, Number_Students, _).

class_preference(Course, Year, Class, Preference) :- class(Course, Year, Class, _, Preference).

class_number(Number, class(_, _, Number, _, _)).

class_preference(Course, Year, Subject, Type, Class, Preference) :-
    teaches(_, Course, Subject, Type, Classes),
    member(Class, Classes),
    class(Course, Year, Class, _, Preference_temp),
    maplist(class_preference(Course, Year), Classes, Preferences),
    solve_preference(Preferences, Preference_temp, Preference).

solve_preference(Preferences, Preference_temp, Preference_temp) :- maplist(=(Preference_temp), Preferences), !.
solve_preference(_, _, any).

%%%%%%%%%%%%%%%%%%%%%%%
%  LESSON PREDICATES  %
%%%%%%%%%%%%%%%%%%%%%%%

lesson_course(lesson(Course, _, _, _, _, _, _), Course).
lesson_course(class_break(Course, _, _, _), Course).
lesson_course_(Course, Lesson) :- lesson_course(Lesson, Course).


lesson_subject(lesson(_, Subject, _, _, _, _, _), Subject).
lesson_year(lesson(Course, Subject, _, _, _, _, _), Year) :- subject_year(Course, Subject, Year).
lesson_year_(Year, lesson(Course, Subject, _, _, _, _, _)) :- subject_year(Course, Subject, Year).

lesson_classes(lesson(_, _, _, _, _, _, Cs), Cs).
lesson_classes_(Cs, lesson(_, _, _, _, _, _, Cs)).



lesson_duration(Course, Subject, Type, Duration) :-
    subject_lesson(Course, Subject, Type, _, _, Duration).

lesson_room_type(Course, Subject, Lesson_Type, Room_Type) :-
    subject_lesson(Course, Subject, Lesson_Type, _, Room_Type, _).

number_lesson_type(Course, Subject, Lesson_Type, Number) :-
    subject_lesson(Course, Subject, Lesson_Type, Number, _, _).

get_all_lessons(Teacher, Lessons) :-
    findall(teaches(Teacher,  Course, Subject, Type, Classes), teaches(Teacher,  Course, Subject, Type, Classes), Lessons).

get_all_rooms(Rooms) :-
    findall(room(Identifier, Type, Capacity), room(Identifier, Type, Capacity), Rooms).

same_day(N1, N2) :- 
    S #= (N1 - 1) / 26,
    E #= (N2 - 1) / 26,
    S #= E.


calc_day(N1, Day) :- Day #= (N1 - 1) / 26. 

is_teacher_break(teacher_break(_, _)).

teaches_to_lesson(teaches(Teacher, Course, Subject, Type, Classes), lesson(Course, Subject, Type, Teacher, task(Start, Duration), task(Start_2, Duration, End_2, Number_Students, Room_Number), Classes), Start_1, Room_Number_1) :-
    lesson_duration(Course, Subject, Type, Duration), 
    Start #= Start_1,
    Start_2 #= Start_1,
    End_2 #= Start_2 + Duration - 1,
    S #= (Start_2 - 1) / 26,
    E #= (End_2 - 1) / 26,
    S #= E,
    Room_Number #= Room_Number_1,
    subject_year(Course, Subject, Year),
    maplist(class_size(Course, Year), Classes, Classes_Size),
    list_sum(Classes_Size, Number_Students).

teaches_to_lesson(teacher_break(Teacher, Day), teacher_break(Teacher, task(Start, Duration)), Start_1, null) :-
    lunch_break(Low_Start_Time_Day, High_Start_Time_Day, Duration), 
    Start #= Start_1,
    Low_Start_Time is Low_Start_Time_Day + (Day * 26),
    High_Start_Time is High_Start_Time_Day + (Day * 26),
    clp_in_interval(Low_Start_Time, High_Start_Time, Start).

teaches_to_lesson(class_break(Class, Day), class_break(Class, task(Start, Duration)), Start_1, null) :-
    lunch_break(Low_Start_Time_Day, High_Start_Time_Day, Duration), 
    Start #= Start_1,
    Low_Start_Time is Low_Start_Time_Day + (Day * 26),
    High_Start_Time is High_Start_Time_Day + (Day * 26),
    clp_in_interval(Low_Start_Time, High_Start_Time, Start).
    
teaches_to_teaches_list(Teaches, ListOfTeaches) :-
    Teaches = teaches(_, Course, Subject, Type, _),
    number_lesson_type(Course, Subject, Type, Number),
    length(ListOfTeaches, Number),
    maplist(=(Teaches), ListOfTeaches).

attends_lesson(Class, lesson(_,_,_,_,_,_,Cs)) :- member(Class, Cs), !.
attends_lesson(Class, class_break(Class, _)).

teaches_lesson(Teacher, lesson(_,_,_, Teacher, _, _, _)).
teaches_lesson(Teacher, teacher_break(Teacher, _)).

teaches_course(teaches(_, Course, _, _, _), Course).
teaches_teacher(teaches(Teacher,  _, _, _, _), Teacher).
teaches_classes(teaches(_,  _, _, _, Classes), Classes).

lesson_task(lesson(_, _, _, _, T, _, _), T) :- !.
lesson_task(teacher_break(_, T), T) :- !.
lesson_task(class_break(_, T), T).

lesson_task_2(lesson(_, _, _, _, _, T, _), T).

lesson_start(lesson(_, _, _, _, task(Start, _), _, _), Start) :- !.
lesson_start(teacher_break(_, task(Start, _)), Start) :- !.
lesson_start(class_break(_, task(Start, _)), Start).

lesson_type(lesson(_, _, Type, _, _, _, _), Type).

room_list_with_machine(Rooms, Rooms_Width_Indexes) :-
    room_list_with_machine(Rooms, Rooms_Width_Indexes, 1).

room_list_with_machine([], [], _).

room_list_with_machine([room(I, T, C) | Tail_1], [room(N, I, T, C) | Tail_2], N) :-
    N1 is N + 1,
    room_list_with_machine(Tail_1, Tail_2, N1).

room_of_type(Type, room(_, _, Type, _)).
room_number(room(Number, _, _, _), Number).

any(_).

morning(Time) :- 
    ((Time) / 13) mod 2 #= 0.

afternoon(Time) :- 
    ((Time) / 13) mod 2 #= 1.

lesson_subject_(X, Y) :- lesson_subject(Y, X).
lesson_type_(X, Y) :- lesson_type(Y, X).

create_teacher_break(Teacher, Breaks) :- 
    length(Breaks, 5), 
    range(0, 4, Days),
    (foreach(Break, Breaks), foreach(Day, Days), param(Teacher) do
        Break = teacher_break(Teacher, Day)
    ).

generate_teachers_lunch_breaks(List, Lunch_Breaks) :-
    maplist(create_teacher_break, List, Lunch_Breaks_Temp),
    append(Lunch_Breaks_Temp, Lunch_Breaks).

create_class_breaks(Course, Year, Class, Breaks) :- 
    length(Breaks, 5), 
    range(0, 4, Days),
    (foreach(Break, Breaks), foreach(Day, Days), param([Class, Course, Year]) do
        Break = class_break(Course, Year, Class, Day)
    ).

generate_classes_lunch_breaks(Course, Year, Classes, Lunch_Breaks) :-
    maplist(create_class_breaks(Course, Year), Classes, Lunch_Breaks_Temp),
    append(Lunch_Breaks_Temp, Lunch_Breaks).

clp_in_interval(Low, High, Val) :-
    Val #>= Low, 
    Val #=< High.

schedule(Lessons, Rooms) :- !,
    get_all_lessons(_, Teaches),
    get_all_rooms(Raw_Rooms),
    
    room_list_with_machine(Raw_Rooms, Rooms),
    
    maplist(teaches_to_teaches_list, Teaches, Teaches_Lists),
    append(Teaches_Lists, All_Teaches),
    
    maplist(teaches_teacher, All_Teaches, Teachers_List), list_to_set(Teachers_List, Teachers),
    maplist(teaches_classes, All_Teaches, Classes_List), append(Classes_List, Classes_Per_Teaches), list_to_set(Classes_Per_Teaches, Classes),
    
    generate_teachers_lunch_breaks(Teachers, Teachers_Lunch_Breaks),
    
    
    
    generate_classes_lunch_breaks(Classes,  Classes_Lunch_Breaks),
    
    append(Teachers_Lunch_Breaks, Classes_Lunch_Breaks, Lunch_Breaks),
    append(All_Teaches, Lunch_Breaks, Intervals),
    
    maplist(teaches_to_lesson, Intervals, Lessons, Starts, All_Rooms),
    include(var, All_Rooms, Lesson_Rooms),
    
    domain(Starts, 1, 130),
    
    write('Processing Rooms...'), nl,
    
    include(is_lesson, Lessons, Actual_Lessons),
    
    write(Actual_Lessons), nl,
    write(Lesson_Rooms), nl,
    write(Rooms), nl,
    
    (foreach(Lesson, Actual_Lessons), foreach(Room, Lesson_Rooms), param(Rooms) do
        Lesson = lesson(Course, Subject, Lesson_Type, _, _, task(_,_,_,Number_St, _), _),
        lesson_room_type(Course, Subject, Lesson_Type, Room_Type),
        include(room_of_type(Room_Type), Rooms, Possible_Rooms),
        maplist(room_number, Possible_Rooms, Domain),
        element(_, Domain, Room),
        nth1(Room, Rooms, room(_, _, _, C)),
        C #>= Number_St
    ),
    
    write('Processing Teachers...'), nl, !,
    
    (foreach(Teacher, Teachers), param(Lessons) do
        include(teaches_lesson(Teacher), Lessons, Teacher_Lessons),
        maplist(lesson_task, Teacher_Lessons, Tasks), 
        write(Tasks), nl,
        disjoint1(Tasks, [])
    ),
    
    write('Processing Classes...'), nl,
    
    exclude(is_teacher_break, Lessons, Classes_Lessons),
    
    maplist(lesson_course, Classes_Lessons, Courses_List), list_to_set(Courses_List, Courses),
    
    (foreach(Course, Courses), param(Classes_Lessons), param(Classes) do
        include(lesson_course_(Course), Classes_Lessons, Course_Lessons),
        
        maplist(lesson_year, Course_Lessons, Course_Years_List), list_to_set(Course_Years_List, Course_Years),
        
        (foreach(Year, Course_Years), param(Course_Lessons) do
            include(lesson_year_(Year), Course_Lessons, Course_Year_Lessons),
            
            maplist(lesson_classes, Course_Year_Lessons, Course_Year_Classes_List), append(Course_Year_Classes_List, Course_Year_Classes_List_1), list_to_set(Course_Year_Classes_List_1, Course_Year_Classes),
            
            (foreach(Class, Course_Year_Classes), param(Course_Year_Lessons) do
                include(attends_lesson(Class), Course_Year_Lessons, Class_Lessons),
                maplist(lesson_task, Class_Lessons, Tasks),
                disjoint1(Tasks, []),
                
                include(is_lesson, Class_Lessons, Actual_Class_Lessons),
                
                (foreach(Lesson, Actual_Class_Lessons), param(Class) do
                        lesson_course(Lesson, Course),
                        subject_year(Course, Subject, Year),
                        lesson_subject(Lesson, Subject),
                        lesson_type(Lesson, Type),
                        lesson_start(Lesson, Start),
                        
                        class_preference(Course, Year, Subject, Type, Class, Preference),
                        call(Preference, Start)
                ),
                
                maplist(lesson_subject, Actual_Class_Lessons, Subjects_List), list_to_set(Subjects_List, Class_Subjects),
                (foreach(Subject, Class_Subjects), param(Actual_Class_Lessons) do
                    include(lesson_subject_(Subject), Actual_Class_Lessons, Class_Subject_Lessons),
                    maplist(lesson_type, Class_Subject_Lessons, Lesson_Types_List), list_to_set(Lesson_Types_List, Lesson_Types),
                    (foreach(Type, Lesson_Types), param(Class_Subject_Lessons) do
                        include(lesson_type_(Type), Class_Subject_Lessons, Class_Subject_Lesson_Type),
                        maplist(lesson_start, Class_Subject_Lesson_Type, Class_Subject_Lesson_Starts),
                        maplist(calc_day, Class_Subject_Lesson_Starts, Class_Subject_Lesson_Day),
                        all_different(Class_Subject_Lesson_Day)
                    )
                 
                )
                
                
            )
        )
    ),
    
    % cumulatives(Lessons_Tasks, Machine_Rooms),
    
    append(Starts, Lesson_Rooms, Vars),
    labeling([], Vars).

lesson_less_than(lesson(_, _, _, _, task(S, _), _, _), lesson(_, _, _, _, task(S1, _), _, _)) :- S < S1. 

lesson_time_in_limit(limits(S, E), lesson(_, _, _, _, task(S1, _), _, _)) :- S1 >= S, S1 =< E.

lessons_filter(_, [], []).
    
lessons_filter(Lessons, [Limit | Tail_Limit], [Lessons_Limit | Tail]) :-
    include(lesson_time_in_limit(Limit), Lessons, Lessons_Limit),
    lessons_filter(Lessons, Tail_Limit, Tail).

mergesort_(Pred, L, L1) :- mergesort(L, L1, Pred).

lesson_room(Room, lesson(_, _, _, _, _, task(_, _, _, _, Room), _)).

write_lesson(Stream, Rooms, start_lesson(lesson(_, Subject, Type, _, _, task(_,_,_,_,Room), _))) :-
    member(room(Room, Ident, _, _), Rooms),
    format(Stream, "~w (~w)[~w],", [Subject, Type, Ident]).

write_lesson(Stream, _, during_lesson(lesson(_, _, _, _, _, _, _))) :-
    format(Stream, "~w,", ['_']).

write_lesson(Stream, _, end_lesson(lesson(_, _, _, _, _, _, _))) :-
    format(Stream, "~w,", ['_']).

write_lesson(Stream, _, Atom) :- format(Stream, "~w,",[Atom]).

lesson_to_string(Lessons, Start, Lesson) :-
    member(LessonTemp, Lessons), lesson(_, _, _, _, task(Start, _), _, _) = LessonTemp, Lesson = start_lesson(LessonTemp), !.

lesson_to_string(Lessons, Start, Lesson) :-
    member(LessonTemp, Lessons), lesson(_, _, _, _, task(Lesson_Start, _), task(_, _, Lesson_End, _, _), _) = LessonTemp, Start > Lesson_Start, Start < Lesson_End, !,
    Lesson = during_lesson(LessonTemp).

lesson_to_string(Lessons, End, Lesson) :-
    member(LessonTemp, Lessons), lesson(_, _, _, _, _, task(_, _, End, _, _), _) = LessonTemp, !,
    Lesson = end_lesson(LessonTemp).

lesson_to_string(_, _, '').

is_odd(Num) :- Num_1 is Num mod 2, Num_1 =:= 1.
is_even(Num) :- Num_1 is Num mod 2, Num_1 =:= 0.

write_hour(Stream, Num) :-
    is_odd(Num), 
    Num_1 is ((Num-1) // 2) + 8,
    write(Stream, Num_1), write(Stream, ':00').

write_hour(Stream, Num) :-
    is_even(Num), 
    Num_1 is ((Num - 1) // 2) + 8,
    write(Stream, Num_1), write(Stream, ':30').
    
    
write_lessons_1(Stream, Rooms, Num, Lessons) :-
    Num_1 is Num + 1,
    write_hour(Stream, Num), 
    write(Stream, ' - '), 
    write_hour(Stream, Num_1), 
    write(Stream, ','), 
    maplist(write_lesson(Stream, Rooms), Lessons), 
    nl(Stream).
    

write_lessons(Lessons, Rooms, FileName) :-
    open(FileName,write, Stream),
    length(L, 5), length(Sizes, 5), maplist(=(26), Sizes), maplist(length, L, Sizes), range(1, 130, Numbers), flatten(L, Numbers), transpose(L, Numbers_L),
    maplist(maplist(lesson_to_string(Lessons)), Numbers_L, Schedule), 
    range(1, 26, Numbers_2),
    format(Stream, ", ~w, ~w, ~w, ~w, ~w~n",['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']),
    maplist(write_lessons_1(Stream, Rooms), Numbers_2, Schedule),
    close(Stream).


write_teacher_lessons(Lessons, Rooms, Teacher) :-
    include(teaches_lesson(Teacher), Lessons, Lessons_1),
    atom_concat(Teacher, '.csv', File_Name),
    write_lessons(Lessons_1, Rooms, File_Name).

attends_lesson(Course, Year, Class, lesson(Course, Subject, _, _, _, _, Cs)) :-
    subject(Course, Year, Subject),
    member(Class, Cs).
    
make_class_file_name(Course, Year, Class, File_Name) :-
    number_codes(Year, YearStr),
    atom_codes(Course, CourseStr),
    number_codes(Class, ClassStr),
    append(YearStr, CourseStr, First),
    append(First, ClassStr, Second),
    append(Second, ".csv", File_Name_Str),
    atom_codes(File_Name, File_Name_Str).

write_class_lessons(Lessons, Rooms, Course, Year, Class) :-
    include(attends_lesson(Course, Year, Class), Lessons, Lessons_1),
    make_class_file_name(Course, Year, Class, File_Name),
    write_lessons(Lessons_1, Rooms, File_Name).

write_room_lessons(Lessons, Rooms, Room) :-
     member(room(Number, Room, _, _), Rooms),
     include(lesson_room(Number), Lessons, Lessons_1),
     atom_concat(Room, '.csv', File_Name),
     write_lessons(Lessons_1, Rooms, File_Name).
    



