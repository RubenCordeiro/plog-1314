:- module(scheduler, [main/0]).
:- use_module(library(clpfd)).
:- use_module(library(lists)).
:- use_module(library(sets)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

list_sum(List, Sum) :-
    list_sum(List, 0, Sum).

list_sum([], Accumulator, Accumulator).

list_sum([Head|Tail], Accumulator, Result) :-
    NewAccumulator is Accumulator + Head,
    list_sum(Tail, NewAccumulator, Result).

range(High, High, [High]) :- !.
range(Low, High, [Low | Tail]) :- Low < High, NewLow is Low + 1, range(NewLow, High, Tail).


flatten(List, FlatList) :-
        flatten(List, [], FlatList0), !,
        FlatList = FlatList0.

flatten(Var, Tl, [Var|Tl]) :-
        var(Var), !.
flatten([], Tl, Tl) :- !.
flatten([Hd|Tl], Tail, List) :- !,
        flatten(Hd, FlatHeadTail, List),
        flatten(Tl, Tail, FlatHeadTail).
flatten(NonList, Tl, [NonList|Tl]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

maplist(Pred, Xs, Ys, Zs, Ws) :-
        (   foreach(X,Xs),
            foreach(Y,Ys),
            foreach(Z,Zs),
            foreach(W,Ws),
            param(Pred)
        do  call(Pred, X, Y, Z, W)
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%
% SUBJECT PREDICATES %
%%%%%%%%%%%%%%%%%%%%%%

subject_year(Course, Subject, Year) :-
    subject(Course, Year, Subject).

%%%%%%%%%%%%%%%%%%%%%%
%  CLASS PREDICATES  %
%%%%%%%%%%%%%%%%%%%%%%

class_size(Course, Year, Class, Number_Students) :-
    class(Course, Year, Class, Number_Students, _).

class_preference(Course, Year, Class, Preference) :- class(Course, Year, Class, _, Preference).

class_number(Number, class(_, _, Number, _, _)).

class_preference(Course, Year, Subject, Type, Class, Preference) :-
    teaches(_, Course, Subject, Type, Classes),
    member(Class, Classes),
    class(Course, Year, Class, _, Preference_temp),
    maplist(class_preference(Course, Year), Classes, Preferences),
    solve_preference(Preferences, Preference_temp, Preference).

solve_preference(Preferences, Preference_temp, Preference_temp) :- maplist(=(Preference_temp), Preferences), !.
solve_preference(_, _, any).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  SUBJECT LESSON PREDICATES  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

lesson_duration(Course, Subject, Type, Duration) :-
    subject_lesson(Course, Subject, Type, _, _, Duration).

lesson_room_type(Course, Subject, Lesson_Type, Room_Type) :-
    subject_lesson(Course, Subject, Lesson_Type, _, Room_Type, _).

number_lesson_type(Course, Subject, Lesson_Type, Number) :-
    subject_lesson(Course, Subject, Lesson_Type, Number, _, _).

%%%%%%%%%%%%%%%%%%%%%%%
%  LESSON PREDICATES  %
%%%%%%%%%%%%%%%%%%%%%%%

lesson_course(lesson(Course, _, _, _, _, _, _, _), Course).
lesson_course(class_break(Course, _, _, _), Course).
lesson_course_(Course, Lesson) :- lesson_course(Lesson, Course).

lesson_subject(lesson(_, Subject, _, _, _, _, _, _), Subject).
lesson_subject_(Subject, Lesson) :- lesson_subject(Lesson, Subject).

lesson_year(lesson(Course, Subject, _, _, _, _, _, _), Year) :- subject_year(Course, Subject, Year).
lesson_year(class_break(_,Year, _, _), Year).
lesson_year_(Year, Lesson) :- lesson_year(Lesson, Year).

lesson_classes(lesson(_, _, _, _, _, _, _, Cs), Cs).
lesson_classes(class_break(_, _, Class, _), [Class]).
lesson_classes_(Classes, Lesson) :- lesson_classes(Lesson, Classes).

is_teacher_break(teacher_break(_, _)).
is_class_break(class_break(_, _, _, _)).
is_lesson(lesson(_, _, _, _, _, _, _, _)).

get_all_lessons(Lessons) :-
    findall(teaches(Teacher,  Course, Subject, Type, Classes), teaches(Teacher,  Course, Subject, Type, Classes), Lessons).

get_all_rooms(Rooms) :-
    findall(room(Identifier, Type, Capacity), room(Identifier, Type, Capacity), Rooms_Temp),
    room_list_with_index(Rooms_Temp, Rooms).

%% Day Management %%

calc_day(N1, Day) :- Day #= (N1 - 1) / 26. 

same_day(N1, N2) :- 
    calc_day(N1, S),
    calc_day(N2, E),
    S #= E.

%% Processing %%

clp_in_interval(Low, High, Val) :-
    Val #>= Low, 
    Val #=< High.

%% Teaches Processing %%

teaches_to_lesson(teaches(Teacher, Course, Subject, Lesson_Type, Classes),
                  lesson(Course, Subject, Lesson_Type, Teacher, task(Start, Duration), Number_Students, Room_Number, Classes),
                  Start_1,
                  Room_Number_1) :-
    lesson_duration(Course, Subject, Lesson_Type, Duration),
    Start #= Start_1, End #= Start + Duration - 1, same_day(Start_1, End),
    Room_Number #= Room_Number_1,
    subject_year(Course, Subject, Year),
    maplist(class_size(Course, Year), Classes, Classes_Size),
    list_sum(Classes_Size, Number_Students).

teaches_to_lesson(teacher_break(Teacher, Day), teacher_break(Teacher, task(Start, Duration)), Start_1, null) :-
    lunch_break(Low_Start_Time_Day, High_Start_Time_Day, Duration),
    Start #= Start_1,
    Low_Start_Time is Low_Start_Time_Day + (Day * 26),
    High_Start_Time is High_Start_Time_Day + (Day * 26),
    clp_in_interval(Low_Start_Time, High_Start_Time, Start).

teaches_to_lesson(class_break(Course, Year, Class, Day), class_break(Course, Year, Class, task(Start, Duration)), Start_1, null) :-
    lunch_break(Low_Start_Time_Day, High_Start_Time_Day, Duration),
    Start #= Start_1,
    Low_Start_Time is Low_Start_Time_Day + (Day * 26),
    High_Start_Time is High_Start_Time_Day + (Day * 26),
    clp_in_interval(Low_Start_Time, High_Start_Time, Start).
    
expand_teaches(Teaches, Expand_Teaches) :-
    Teaches = teaches(_, Course, Subject, Type, _),
    number_lesson_type(Course, Subject, Type, Number),
    length(Expand_Teaches, Number),
    maplist(=(Teaches), Expand_Teaches).

expand_teaches_list(Teaches_List, Expanded_Teaches_List) :-
    maplist(expand_teaches, Teaches_List, Expanded_Teaches_List_Temp),
    append(Expanded_Teaches_List_Temp, Expanded_Teaches_List).

attends_lesson(Course, Year, Class, lesson(Course, Subject, _, _, _, _, _, Classes)) :- 
    member(Class, Classes),
    subject_year(Course, Subject, Year), !.

attends_lesson(Course, Year, Class, class_break(Course, Year, Class, _)).


teaches_lesson(Teacher, lesson(_, _, _, Teacher, _, _, _, _)) :- !.
teaches_lesson(Teacher, teacher_break(Teacher, _)).

teaches_course(teaches(_, Course, _, _, _), Course).
teaches_course_(Course, Teaches) :- teaches_course(Teaches, Course).

teaches_teacher(teaches(Teacher, _, _, _, _), Teacher).
teaches_classes(teaches(_, _, _, _, Classes), Classes).

teaches_year(teaches(_, Course, Subject, _, _), Year) :- subject_year(Course, Subject, Year).
teaches_year_(Year, Teaches) :- teaches_year(Teaches, Year).

lesson_task(lesson(_, _, _, _, Task, _, _, _), Task) :- !.
lesson_task(teacher_break(_, Task), Task) :- !.
lesson_task(class_break(_, _, _, Task), Task).

lesson_start(Lesson, Start) :- lesson_task(Lesson, task(Start, _)).

lesson_type(lesson(_, _, Lesson_Type, _, _, _, _, _), Lesson_Type).
lesson_type_(Lesson_Type, Lesson) :- lesson_type(Lesson, Lesson_Type).

lesson_room(lesson(_, _, _, _, _, _, Room, _), Room).
lesson_room_(Room, lesson(_, _, _, _, _, _, Room, _)).

lesson_to_big_task(lesson(_, _, _, _, task(Start, Duration), _, Room_Number, _), task(Start_1, Duration, _, 1, Room_Number_1)) :-
    Start_1 #= Start, Room_Number_1 #= Room_Number.

room_to_machine(room(Number, _, _, _), machine(Number, 1)).

%% Room Processing %%

room_list_with_index(Rooms, Rooms_With_Indexes) :-
    room_list_with_index(Rooms, Rooms_With_Indexes, 1).

room_list_with_index([], [], _).

room_list_with_index([room(I, T, C) | Tail_Rooms], [room(N, I, T, C) | Tail_Rooms_With_Indexes], N) :-
    N1 is N + 1,
    room_list_with_index(Tail_Rooms, Tail_Rooms_With_Indexes, N1).

room_type(room(_, _, Type, _), Type).
room_type_(Type, Room) :- room_type(Room, Type).

room_number(room(Number, _, _, _), Number).
room_capacity(room(_, _, _, Capacity), Capacity).
 
%% Filters

any(_).

morning(Slot) :-
    calc_day(Slot, Day),
    Day_Hour #= Slot - Day * 26,
    lunch_break(_, Separator, _),
    Day_Hour #=< Separator.

afternoon(Slot) :-
    calc_day(Slot, Day),
    Day_Hour #= Slot - Day * 26,
    lunch_break(_, Separator, _),
    Day_Hour #> Separator.

%% Lunch Breaks

generate_teachers_lunch_breaks(List, Lunch_Breaks) :-
    maplist(create_teacher_breaks, List, Lunch_Breaks_Temp),
    append(Lunch_Breaks_Temp, Lunch_Breaks).

generate_classes_lunch_breaks(Course, Year, Classes, Lunch_Breaks) :-
    maplist(create_class_breaks(Course, Year), Classes, Lunch_Breaks_Temp),
    append(Lunch_Breaks_Temp, Lunch_Breaks).

create_teacher_breaks(Teacher, Breaks) :-
    length(Breaks, 5),
    range(0, 4, Days),
    (foreach(Break, Breaks), foreach(Day, Days), param(Teacher) do
        Break = teacher_break(Teacher, Day)
    ).

create_class_breaks(Course, Year, Class, Breaks) :-
    length(Breaks, 5),
    range(0, 4, Days),
    (foreach(Break, Breaks), foreach(Day, Days), param([Course, Year, Class]) do
        Break = class_break(Course, Year, Class, Day)
    ).

%% scheduling

generate_year_lunch_breaks(Course, Teaches_List, Year, Year_Lunch_Breaks) :-
    include(teaches_year_(Year), Teaches_List, Course_Year_Teaches),
    maplist(teaches_classes, Course_Year_Teaches, Course_Year_Classes_Temp), append(Course_Year_Classes_Temp, Course_Year_Classes_Temp_1),
    list_to_set(Course_Year_Classes_Temp_1, Course_Year_Classes),
    generate_classes_lunch_breaks(Course, Year, Course_Year_Classes, Year_Lunch_Breaks).

generate_course_lunch_breaks(Teaches_List, Course, Course_Lunch_Breaks) :-
    maplist(teaches_year, Teaches_List, Course_Years_Temp), list_to_set(Course_Years_Temp, Course_Years),
    include(teaches_course_(Course), Teaches_List, Course_Teaches),
    maplist(generate_year_lunch_breaks(Course, Course_Teaches), Course_Years, Course_Lunch_Breaks_Temp), 
    append(Course_Lunch_Breaks_Temp, Course_Lunch_Breaks).    

schedule(Lessons, Rooms) :-
    get_all_lessons(Teaches_List),
    get_all_rooms(Rooms),
    
    maplist(teaches_teacher, Teaches_List, Teachers_Temp), list_to_set(Teachers_Temp, Teachers),
    maplist(teaches_course, Teaches_List, Courses_Temp), list_to_set(Courses_Temp, Courses),
    
    expand_teaches_list(Teaches_List, Expanded_Teaches_List),
    
    generate_teachers_lunch_breaks(Teachers, Teachers_Lunch_Breaks),
    maplist(generate_course_lunch_breaks(Teaches_List), Courses, Classes_Lunch_Breaks_Temp), append(Classes_Lunch_Breaks_Temp, Classes_Lunch_Breaks),
    
    append(Teachers_Lunch_Breaks, Classes_Lunch_Breaks, Lunch_Breaks),
    append(Lunch_Breaks, Expanded_Teaches_List, All_Teaches),
    
    maplist(teaches_to_lesson, All_Teaches, Lessons, Starts, All_Rooms),
    
    domain(Starts, 1, 130),
    
    include(var, All_Rooms, Lesson_Rooms),
    include(is_lesson, Lessons, Actual_Lessons),
    
    format("Processing rooms...~n", []),
    
    (foreach(Lesson, Actual_Lessons), param([Actual_Lessons, Rooms]) do
        Lesson = lesson(Course, Subject, Lesson_Type, _, _, Number_Students, Room_Number, _),
        lesson_room_type(Course, Subject, Lesson_Type, Room_Type),
        include(room_type_(Room_Type), Rooms, Possible_Rooms),
        maplist(room_number, Possible_Rooms, Possible_Rooms_Number),
        element(_, Possible_Rooms_Number, Room_Number),
        maplist(room_capacity, Rooms, Capacities),
        element(Room_Number, Capacities, Room_Capacity),
        Room_Capacity #>= Number_Students
    ),
    
    maplist(room_to_machine, Rooms, Rooms_Machines),
    maplist(lesson_to_big_task, Actual_Lessons, Lessons_Big_Tasks),
    
    cumulatives(Lessons_Big_Tasks, Rooms_Machines, [bound(upper)]),  
    
    format("Processing Teachers...~n", []),
    
    exclude(is_class_break, Lessons, Teachers_Lessons),
    
    (foreach(Teacher, Teachers), param(Teachers_Lessons) do
        include(teaches_lesson(Teacher), Teachers_Lessons, Teacher_Lessons),
        maplist(lesson_task, Teacher_Lessons, Tasks),
        disjoint1(Tasks, [])
    ),
    
    format("Processing Classes...~n", []),
    
    exclude(is_teacher_break, Lessons, Classes_Lessons),
    
    (foreach(Course, Courses), param(Classes_Lessons) do
        include(lesson_course_(Course), Classes_Lessons, Course_Lessons),
        maplist(lesson_year, Course_Lessons, Course_Years_List), list_to_set(Course_Years_List, Course_Years),

        (foreach(Year, Course_Years), param([Course, Course_Lessons]) do
            include(lesson_year_(Year), Course_Lessons, Course_Year_Lessons),
             maplist(lesson_classes, Course_Year_Lessons, Course_Year_Classes_List), append(Course_Year_Classes_List, Course_Year_Classes_List_1), list_to_set(Course_Year_Classes_List_1, Course_Year_Classes),
             
             (foreach(Class, Course_Year_Classes), param([Course, Year, Course_Year_Lessons]) do
                 include(attends_lesson(Course, Year, Class), Course_Year_Lessons, Class_Lessons_With_Breaks),
                 maplist(lesson_task, Class_Lessons_With_Breaks, Tasks),
                 disjoint1(Tasks, []),
                 
                 include(is_lesson, Class_Lessons_With_Breaks, Class_Lessons),
                 
                 (foreach(Lesson, Class_Lessons), param([Course, Year, Class]) do
                     lesson_subject(Lesson, Subject),
                     lesson_type(Lesson, Type),
                     lesson_start(Lesson, Start),
                     class_preference(Course, Year, Subject, Type, Class, Preference),
                     call(Preference, Start)
                 ),
                 
                 maplist(lesson_subject, Class_Lessons, Subject_List), list_to_set(Subject_List, Class_Subjects),
                 (foreach(Subject, Class_Subjects), param(Class_Lessons) do
                     include(lesson_subject_(Subject), Class_Lessons, Class_Subject_Lessons),
                     maplist(lesson_type, Class_Subject_Lessons, Lesson_Types_List), list_to_set(Lesson_Types_List, Lesson_Types),
                     (foreach(Type, Lesson_Types), param(Class_Subject_Lessons) do
                         include(lesson_type_(Type), Class_Subject_Lessons, Class_Subject_Lesson_Type),
                         maplist(lesson_start, Class_Subject_Lesson_Type, Class_Subject_Lesson_Starts),
                         maplist(calc_day, Class_Subject_Lesson_Starts, Class_Subject_Lesson_Day),
                         all_different(Class_Subject_Lesson_Day)
                     )
                 )
             )
        )
    ),
    
    append(Starts, Lesson_Rooms, Vars),
    labeling([], Vars).

%% Output

write_lesson(Stream, Rooms, start_lesson(lesson(_, Subject, Type, Teacher, _, _, Room, _))) :-
    member(room(Room, Ident, _, _), Rooms),
    format(Stream, "~w (~w)[~w]{~w},", [Subject, Type, Ident, Teacher]).

write_lesson(Stream, _, during_lesson(lesson(_, _, _, _, _, _, _, _))) :-
    format(Stream, "~w,", ['_']).

write_lesson(Stream, _, end_lesson(lesson(_, _, _, _, _, _, _, _))) :-
    format(Stream, "~w,", ['_']).

write_lesson(Stream, _, Atom) :- format(Stream, "~w,",[Atom]).

lesson_to_string(Lessons, Time, Lesson) :-
    member(LessonTemp, Lessons), lesson(_, _, _, _, task(Time, _), _, _, _) = LessonTemp, Lesson = start_lesson(LessonTemp), !.

lesson_to_string(Lessons, Time, Lesson) :-
    member(LessonTemp, Lessons), lesson(_, _, _, _, task(Lesson_Start, Duration), _, _, _) = LessonTemp, Time > Lesson_Start, Time < (Lesson_Start + Duration - 1), !,
    Lesson = during_lesson(LessonTemp).

lesson_to_string(Lessons, Time, Lesson) :-
    member(LessonTemp, Lessons), lesson(_, _, _, _, task(Lesson_Start, Duration), _, _, _) = LessonTemp, Time =:= (Lesson_Start + Duration - 1), !,
    Lesson = end_lesson(LessonTemp).

lesson_to_string(_, _, '').

is_odd(Num) :- Num_1 is Num mod 2, Num_1 =:= 1.
is_even(Num) :- Num_1 is Num mod 2, Num_1 =:= 0.

write_hour(Stream, Num) :-
    is_odd(Num), 
    Num_1 is ((Num-1) // 2) + 8,
    write(Stream, Num_1), write(Stream, ':00').

write_hour(Stream, Num) :-
    is_even(Num), 
    Num_1 is ((Num - 1) // 2) + 8,
    write(Stream, Num_1), write(Stream, ':30').
    
    
write_lessons_1(Stream, Rooms, Num, Lessons) :-
    Num_1 is Num + 1,
    write_hour(Stream, Num), 
    write(Stream, ' - '), 
    write_hour(Stream, Num_1), 
    write(Stream, ','), 
    maplist(write_lesson(Stream, Rooms), Lessons), 
    nl(Stream).
    

write_lessons(Lessons, Rooms, FileName) :-
    open(FileName,write, Stream),
    length(L, 5), length(Sizes, 5), maplist(=(26), Sizes), maplist(length, L, Sizes), range(1, 130, Numbers), flatten(L, Numbers), transpose(L, Numbers_L),
    maplist(maplist(lesson_to_string(Lessons)), Numbers_L, Schedule), 
    range(1, 26, Numbers_2),
    format(Stream, ", ~w, ~w, ~w, ~w, ~w~n",['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']),
    maplist(write_lessons_1(Stream, Rooms), Numbers_2, Schedule),
    close(Stream).


write_teacher_lessons(Lessons, Rooms, Teacher) :-
    include(teaches_lesson(Teacher), Lessons, Lessons_1),
    atom_concat(Teacher, '.csv', File_Name),
    write_lessons(Lessons_1, Rooms, File_Name).
    
make_class_file_name(Course, Year, Class, File_Name) :-
    number_codes(Year, YearStr),
    atom_codes(Course, CourseStr),
    number_codes(Class, ClassStr),
    append(YearStr, CourseStr, First),
    append(First, ClassStr, Second),
    append(Second, ".csv", File_Name_Str),
    atom_codes(File_Name, File_Name_Str).

write_class_lessons(Lessons, Rooms, Course, Year, Class) :-
    include(attends_lesson(Course, Year, Class), Lessons, Lessons_1),
    make_class_file_name(Course, Year, Class, File_Name),
    write_lessons(Lessons_1, Rooms, File_Name).

write_room_lessons(Lessons, Rooms, Number) :- integer(Number), !,
     member(room(Number, Room, _, _), Rooms),
     include(lesson_room_(Number), Lessons, Lessons_1),
     atom_concat(Room, '.csv', File_Name),
     write_lessons(Lessons_1, Rooms, File_Name).

write_room_lessons(Lessons, Rooms, Room) :-
     member(room(Number, Room, _, _), Rooms),
     include(lesson_room_(Number), Lessons, Lessons_1),
     atom_concat(Room, '.csv', File_Name),
     write_lessons(Lessons_1, Rooms, File_Name).

write_all_lessons(Lessons, Rooms) :-
    exclude(is_class_break, Lessons, Teachers_Lessons),
    maplist(teaches_lesson, Teachers_List, Teachers_Lessons), list_to_set(Teachers_List, Teachers),
    maplist(write_teacher_lessons(Lessons, Rooms), Teachers),
    exclude(is_teacher_break, Teachers_Lessons, Rooms_Lessons),
    maplist(lesson_room, Rooms_Lessons, Rooms_List), list_to_set(Rooms_List, Rooms_Numbers),
    maplist(write_room_lessons(Lessons, Rooms), Rooms_Numbers),
    exclude(is_teacher_break, Lessons, Classes_Lessons),
    maplist(lesson_course, Classes_Lessons, Courses_List), list_to_set(Courses_List, Courses),
    (foreach(Course, Courses), param([Classes_Lessons, Lessons, Rooms]) do
        include(lesson_course_(Course), Classes_Lessons, Course_Lessons),
        maplist(lesson_year, Course_Lessons, Course_Year_List), list_to_set(Course_Year_List, Course_Years),
        (foreach(Year, Course_Years), param([Course_Lessons, Lessons, Rooms, Course]) do
            include(lesson_year_(Year), Course_Lessons, Course_Year_Lessons),
            maplist(lesson_classes, Course_Year_Lessons, Classes_List), append(Classes_List, Classes_List_1), list_to_set(Classes_List_1, Classes),
            maplist(write_class_lessons(Lessons, Rooms, Course, Year), Classes)
        )
    ).

check(Lessons, Rooms) :-
    include(is_lesson, Lessons, Lessons_1), 
    (foreach(room(Number, _, _, _), Rooms), param(Lessons_1) do !,
        include(lesson_room_(Number), Lessons_1, Lessons),
        maplist(lesson_start, Lessons, Starts),
        write(Starts), nl,
        all_different(Starts)
    ).

run(FileName) :-
    format("~nConsulting file...~n", []),
    consult(FileName),
    format("~nGenerating Schedules...~n", []),
    schedule(L, R),
    statistics(runtime, [Miliseconds| _]),
    format("~nWriting Files...~n", []),
    write_all_lessons(L, R), 
    nl, 
    format("~nStatistics:~n", []),
    fd_statistics,
    format("Scheduling Time: ~w ms.~n", [Miliseconds]).
    

main :-
    current_prolog_flag(argv, Args),
    length(Args, N),
    N >= 1,
    Args = [FileName|_],
    !, run(FileName).

main :-
    write('Insert data file name: '),
    read(FileName),
    run(FileName).

user:runtime_entry(start) :-
    main.


