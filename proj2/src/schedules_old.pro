:- use_module(library(clpfd)).
:- use_module(library(lists)).
:- use_module(library(sets)).

% A lesson is a task that takes one teacher
% A lesson is a task that takes one room of a specified type
% A lesson is a task that takes one class
% A lesson is a task that consumes it's duration of a teacher (machine) resources.
% No two lessons of the same subject of the same class can be on the same day
% The capacity of a room is >= size of the class

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% KB
%% generate([school_year]).
%%
%% school_year([subject], [class]).
%%
%% class(Number_Of_Students)
%%
%% subject(Name, (Number_T, Room_Type_T, Dur_T), (Number_TP, Room_Type_TP, Dur_TP), (Number_PL, Room_Type_PL, Dur_PL))
%%
%% teaches(Teacher, Subject_Name, Lesson_Type, [Class_Number])
%%
%% room(Identifier, Room_Type, Capacity)


%% room(Identifier, Room_Type, Capacity)
%% subject(Course, Year, Semester, Name, (Number_T, Room_Type_T, Dur_T), (Number_TP, Room_Type_TP, Dur_TP), (Number_PL, Room_Type_PL, Dur_PL)) 
%% class(Course, Year, Class_Number, Number_Students)
%% teaches(Teacher, Course, Subject_Name, Lesson_Type, [Class_Number])


room('B001', 'Amphitheater', 184).
room('B002', 'Amphitheater', 184).
room('B003', 'Amphitheater', 184).
room('B004', 'Amphitheater', 99).
room('B203', 'Informatics', 20).

subject('MIEIC', 1, 1, 'FPRO', (2, 'Amphitheater', 3), (1, 'Informatics', 4),   (0, _, _)).
subject('MIEIC', 1, 1, 'AMAT', (2, 'Amphitheater', 3), (1, 'Normal', 4),        (0, _, _)).
subject('MIEIC', 1, 1, 'ALGE', (2, 'Amphitheater', 3), (1, 'Normal', 4),        (0, _, _)).
subject('MIEIC', 1, 1, 'AOCO', (2, 'Amphitheater', 3), (1, 'Normal', 4),        (0, _, _)).
subject('MIEIC', 1, 1, 'MDIS', (1, 'Amphitheater', 4), (1, 'Informatics', 4),   (0, _, _)).

class('MIEIC', 1, 1, 20).
class('MIEIC', 1, 2, 20).
class('MIEIC', 1, 3, 20).
class('MIEIC', 1, 4, 20).
class('MIEIC', 1, 5, 20).
class('MIEIC', 1, 6, 20).

teaches('AFCC', 'MIEIC', 'FPRO', 'T', [1, 2, 3, 4, 5, 6]).

teaches('RCS',  'MIEIC', 'FPRO', 'TP', [1]).
teaches('JJ',   'MIEIC', 'FPRO', 'TP', [2]).
teaches('RCS',  'MIEIC', 'FPRO', 'TP', [3]).
teaches('JGB',  'MIEIC', 'FPRO', 'TP', [4]).
teaches('AFCC', 'MIEIC', 'FPRO', 'TP', [5]).
teaches('JJ',   'MIEIC', 'FPRO', 'TP', [6]).

teaches('AMF', 'MIEIC', 'ALGE', 'T', [1, 2, 3, 4, 5, 6]).

teaches('AMF',  'MIEIC', 'ALGE', 'TP', [1]).
teaches('AMF',  'MIEIC', 'ALGE', 'TP', [2]).
teaches('AMF',  'MIEIC', 'ALGE', 'TP', [3]).
teaches('CMCR', 'MIEIC', 'ALGE', 'TP', [4]).
teaches('AMF',  'MIEIC', 'ALGE', 'TP', [5]).
teaches('CMCR', 'MIEIC', 'ALGE', 'TP', [6]).

teaches('RMV', 'MIEIC', 'AOCO', 'T', [1, 2, 3, 4, 5, 6]).

teaches('JCF',   'MIEIC', 'AOCO', 'TP', [1]).
teaches('CMBNS', 'MIEIC', 'AOCO', 'TP', [2]).
teaches('AJA',   'MIEIC', 'AOCO', 'TP', [3]).
teaches('JCF',   'MIEIC', 'AOCO', 'TP', [4]).
teaches('JCF',   'MIEIC', 'AOCO', 'TP', [5]).
teaches('JCF',   'MIEIC', 'AOCO', 'TP', [6]).

teaches('AMF', 'MIEIC', 'AMAT', 'T', [1, 2, 3, 4, 5, 6]).

teaches('TCRD',    'MIEIC', 'AMAT', 'TP', [4]).
teaches('AMAN',    'MIEIC', 'AMAT', 'TP', [5]).
teaches('AMAN',    'MIEIC', 'AMAT', 'TP', [3]).
teaches('JaimeDR', 'MIEIC', 'AMAT', 'TP', [2]).
teaches('JaimeDR', 'MIEIC', 'AMAT', 'TP', [6]).
teaches('JaimeDR', 'MIEIC', 'AMAT', 'TP', [1]).

teaches('GTD', 'MIEIC', 'MDIS', 'T', [1, 2, 3, 4, 5, 6]).

teaches('EDMR',  'MIEIC', 'MDIS', 'TP', [1]).
teaches('EDMR',  'MIEIC', 'MDIS', 'TP', [2]).
teaches('GTD',   'MIEIC', 'MDIS', 'TP', [3]).
teaches('GTD',   'MIEIC', 'MDIS', 'TP', [4]).
teaches('EDMR',  'MIEIC', 'MDIS', 'TP', [5]).
teaches('CMC',   'MIEIC', 'MDIS', 'TP', [6]).


teaches(Teacher, Subject) :- 
        teaches(Teacher, Subject, _,_, _).

is_teacher(Teacher) :- teaches(Teacher, _).

%% lesson(Teacher, Subject, Lesson_Type, [Class], Start_Time, Duration, Room) 

%% Por dia h� 25 timeslots de 30 minutos cada - [8:00 - 8:30] -> [20:00 - 20:30]
%% Cada docente lecciona 20 timeslots por semana -> 10 horas de aulas

create_lesson(lesson(Teacher, Course, Subject, Lesson_Type, Classes, Start_Time, Duration, Room), teaches(Teacher, Course, Subject, Lesson_Type, Classes), Start_Time, Duration, Room).

create_lessons([], [], [], [], []).

create_lessons([L | L_T], [T | T_T], [ST | ST_T], [D| D_T], [R | R_T]) :-
    create_lesson(L, T, ST, D, R),
    create_lessons(L_T, T_T, ST_T, D_T, R_T).

get_all_lessons(Teacher, Lessons) :-
    findall(teaches(Teacher, Course, Subject, Type, Classes), teaches(Teacher, Course, Subject, Type, Classes), Lessons).

create_task(task(St, D), St, D).

create_lesson_task(Teachers, lesson(Teacher, _, _, _, _, Start_Time, Duration, _), task(Start, T_Duration, _, 1, T_Teacher)) :-
    nth1(T_Teacher, Teachers, Teacher),
    Start #= Start_Time,
    T_Duration #= Duration.

create_teacher_machine(Teachers, Teacher, machine(Name, 1)) :-
    nth1(Name, Teachers, Teacher).

get_all_teachers(Teachers) :-
    setof(T, is_teacher(T), Teachers).

get_lesson_duration(Course, Name, 'T', Duration) :-
    subject(Course, _, _, Name, (_, _, Duration), _, _).

get_lesson_duration(Course, Name, 'TP', Duration) :-
    subject(Course, _, _, Name, _, (_, _, Duration), _).

get_lesson_duration(Course, Name, 'PL', Duration) :-
    subject(Course, _, _, Name, _, _, (_, _, Duration)).

get_lesson_duration(teaches(_, Course, Name, Type, _), Duration) :-
    get_lesson_duration(Course, Name, Type, Duration).

create_class_lesson(Start_Time, Duration, Class, class_lesson(Class, task(St, Dur))) :- St #= Start_Time, Dur #= Duration.

lesson_to_class_lesson(teaches(_, _, _, _, Classes), Start_Time, Duration, Lessons) :-
    St #= Start_Time, D #= Duration,
    maplist(create_class_lesson(St, D), Classes, Lessons).

create_element(Start, Duration, task(St, Dur)) :- St #= Start, Dur #= Duration.

class_lesson_task(class_lesson(_, T), T).

teaches_to_lesson(teaches(Teacher, Course, Subject, Type, Classes), lesson(Course, Subject, Type, Teacher, task(Start, Duration), Classes), Start_1) :-
    get_lesson_duration(Course, Subject, Type, Duration), Start #= Start_1.

attends_lesson(Class, lesson(_,_,_,_,_,Cs)) :- member(Class, Cs).
teaches_lesson(Teacher, lesson(_,_,_, Teacher, _, _)).
lesson_task(lesson(_, _, _, _, T, _), T).

schedule(Lessons) :-
    get_all_lessons(_, Teaches),
    same_length(Teaches, Starts),
    domain(Starts, 1, 125),
    maplist(teaches_to_lesson, Teaches, Lessons, Starts),
    
    maplist(teaches_lesson, Teachers_List, Lessons), list_to_set(Teachers_List, Teachers),
    write(Teachers), nl,
    
    (foreach(Teacher, Teachers), param(Lessons) do
        include(teaches_lesson(Teacher), Lessons, Teacher_Lessons),
        maplist(lesson_task, Teacher_Lessons, Tasks),
        disjoint1(Tasks, [])
    ),
    
    setof(C, (member(lesson(_, _, _, _, _, Cs), Lessons), member(C, Cs)), Classes),
    
    (foreach(Class, Classes), param(Lessons) do
        include(attends_lesson(Class), Lessons, Class_Lessons),
        maplist(lesson_task, Class_Lessons, Tasks),
        disjoint1(Tasks, [])
    ),  
    
    labeling([], Starts).
    
get_class_subjects(Course, Year, Semester, Class, Subjects) :-
    findall(Subject, (teaches(_, Course, Subject, _, Classes), subject(Course, Year, Semester, Subject, _, _, _), member(Class, Classes)), Lessons), list_to_set(Lessons, Subjects).

get_classes(Course, Year, Classes) :-
    findall(Class, class(Course, Year, Class, _), Classes).

main :-
    Starts = [S1, S2, S3, S4, S5],
    domain(Starts, 1, 125),    
    Lessons = 
        [
          task(S1, 1),
          task(S2, 2),
          task(S3, 3),
          task(S4, 2),
          task(S5, 1)   
        ],
    
    disjoint1(Lessons),
    
    labeling([], Starts),
    
    write(Starts), nl.

