library(lists).

print_horizontal_bar :- put_code(0x2500).
print_horizontal_middle_top_connection :- put_code(0x252C).
print_horizontal_middle_bottom_connection :- put_code(0x2534).
print_middle_connection :- put_code(0x253C).
print_vertical_bar :- put_code(0x2502).
print_vertical_middle_left_connection :- put_code(0x251C).
print_vertical_middle_right_connection :- put_code(0x2524).
print_top_left_corner :- put_code(0x250C).
print_top_right_corner :- put_code(0x2510).
print_bottom_left_corner :- put_code(0x2514).
print_bottom_right_corner :- put_code(0x2518).

print_white_spaces(1) :- !, put_char(' ').
print_white_spaces(N) :- put_char(' '), N1 is N - 1, print_white_spaces(N1).

create_board(NumRows, NumCols, Board) :-
	board_dimensions(Board, NumRows, NumCols).

% initialize_board(+NumRows, +NumCols, -Board)
initialize_board(NumRows, NumCols, Board) :-
	board_dimensions(Board, NumRows, NumCols),
	initialize_rows(Board).
	
	
initialize_rows(Board) :-
	append([Row1, Row2 | BlankRows], [Row2Last, RowLast], Board),
	initialize_row_alternate(black, circle, Row1), 
	initialize_row_alternate(black, square, Row2),
	initialize_row_alternate(white, circle, Row2Last), 
	initialize_row_alternate(white, square, RowLast),
	initialize_each_row(BlankRows).
	
initialize_row_alternate(_, _, []).
	
initialize_row_alternate(Colour, Piece, [cell(Colour, [Piece]) | RestOfRow]) :-
	different_pieces(Piece, OtherPiece),
	initialize_row_alternate(Colour, OtherPiece, RestOfRow). 
	
initialize_each_row([]).
	
initialize_each_row([Row | RestOfRows]) :-
	intialize_each_element(Row),
	initialize_each_row(RestOfRows).
	

intialize_each_element([]).
	
intialize_each_element([cell(blank) | RestOfRow]) :-
	intialize_each_element(RestOfRow).

	
is_piece(square).
is_piece(circle).

different_pieces(square, circle).
different_pieces(circle, square).
	
is_valid_colour(black).
is_valid_colour(white).

different_valid_colour(black, white).
different_valid_colour(white, black).
	
is_valid_elem_list([Elem1]) :- is_piece(Elem1).
is_valid_elem_list([Elem1, Elem2]) :- is_piece(Elem1), is_valid_elem_list([Elem2]).
is_valid_elem_list([Elem1, Elem2, Elem3]) :- is_piece(Elem1), is_valid_elem_list([Elem2, Elem3]).

cell(blank).
is_cell_blank(cell(blank)).

cell(PieceColour, Elems) :- is_valid_colour(PieceColour), is_valid_elem_list(Elems).

is_cell(cell(blank)).
is_cell(cell(Color, Elems)) :- cell(Color, Elems).

number_of_board_rows(Board, N) :- length(Board, N).
number_of_board_columns([], _).
number_of_board_columns([FirstRow | RestOfRows], N) :- length(FirstRow, N), number_of_board_columns(RestOfRows, N).

board_dimensions(Board, N_Rows, N_Columns) :- number_of_board_rows(Board, N_Rows), number_of_board_columns(Board, N_Columns).

%%%%%%%%%%%%%%%%%
%% PRINT_BOARD %%
%%%%%%%%%%%%%%%%%

column_letter(Column, Letter) :-
	integer(Column), !,
	char_code(a, FirstColumnCharCode),
	ColumnCharCode is FirstColumnCharCode + Column - 1,
	char_code(Letter, ColumnCharCode).

column_letter(Column, Letter) :-
	char_code(a, FirstColumnCharCode),
	char_code(Letter, ColumnCharCode),
	Column is ColumnCharCode - FirstColumnCharCode + 1.

print_column_letters(Board) :-
	number_of_board_columns(Board, NCols),
	print_white_spaces(1),
	print_column_letters_aux(1, NCols), nl.
	
print_column_letters_aux(Number_of_columns, Number_of_columns) :- 
	!, 
	column_letter(Number_of_columns, Letter),
	print_white_spaces(2),
	write(Letter).
	
print_column_letters_aux(Column, Number_of_columns) :-
	column_letter(Column, Letter),
	print_white_spaces(2),
	write(Letter),
	print_white_spaces(3),
	Next_column is Column + 1,
	print_column_letters_aux(Next_column, Number_of_columns).
	

print_board_top(Board) :-
	number_of_board_columns(Board, NCols),
	N is NCols - 1,
	print_top_left_corner,
	print_board_top_aux(N), nl.
	
print_board_bottom(Board) :-
	number_of_board_columns(Board, NCols),
	N is NCols - 1,
	print_bottom_left_corner,
	print_board_bottom_aux(N), nl.

print_board_top_aux(0) :- print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_top_right_corner.
print_board_top_aux(N) :-
	print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_horizontal_middle_top_connection, N1 is N - 1, print_board_top_aux(N1).
	
print_board_bottom_aux(0) :- print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_bottom_right_corner.
print_board_bottom_aux(N) :-
	print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_horizontal_middle_bottom_connection, N1 is N - 1, print_board_bottom_aux(N1).
	
print_board(Board) :-
	print_white_spaces(3), print_column_letters(Board),
	print_white_spaces(3), print_board_top(Board),
	print_board_rows(Board, 1),
	print_white_spaces(3), print_board_bottom(Board), !.	

print_row([], _) :- print_vertical_bar.
	
print_row([Head | Tail], ElemNum) :-
	print_vertical_bar, print_white_spaces(2), print_cell_element(Head, ElemNum), print_white_spaces(2), print_row(Tail, ElemNum).

print_row_separator([]).
	
print_row_separator([_]) :-
	print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_vertical_middle_right_connection.

print_row_separator([_ | Tail]) :-
	print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_horizontal_bar, print_middle_connection,
	print_row_separator(Tail).

print_board_rows([]).
	
print_board_rows([Row], N) :- 
	print_white_spaces(3), print_row(Row, 3), nl, 
	print_white_spaces(1), write(N), print_white_spaces(1), print_row(Row, 2), nl, 
	print_white_spaces(3), print_row(Row, 1), nl.
	
print_board_rows([Row | RestOfRows], N) :-
	print_white_spaces(3), print_row(Row, 3), nl, 
	print_white_spaces(1), write(N), print_white_spaces(1), print_row(Row, 2), nl, 
	print_white_spaces(3), print_row(Row, 1), nl,
	print_white_spaces(3), print_vertical_middle_left_connection, print_row_separator(Row), nl,
	N1 is N + 1,
	print_board_rows(RestOfRows, N1).

%%%%%%%%%%%%%%%%
%% PRINT_CELL %%
%%%%%%%%%%%%%%%%

print_cell_element(cell(_, []), N) :- N >= 1, N =< 3, print_piece(blank).

print_cell_element(cell(Colour, [Head | _]), 1) :-
	print_piece(Colour, Head).
	
print_cell_element(cell(Colour, [_ | Tail]), N) :-
	N =< 3,
	N >= 1,
	N1 is N - 1,
	print_cell_element(cell(Colour, Tail), N1).

print_cell_element(cell(blank), N) :- N >= 1, N =< 3, print_piece(blank).
	
print_piece(blank) :- print_white_spaces(1).
print_piece(white, circle) :- put_code(0x26AA).
print_piece(black, circle) :- put_code(0x2B24).
print_piece(white, square) :- put_code(0x2B1C).
print_piece(black, square) :- put_code(0x25A0).
	

say_board([]).
	
say_board([Row | Rest]) :-
	say_board_row(Row), nl,
	say_board(Rest).
	
say_board_row([]).
	
say_board_row([Elem | Rest]) :-
	print(Elem), print(','), say_board_row(Rest).
	
cell_at(NumRow, NumColumn, Board, Cell) :-
	nth1(NumRow, Board, Row),
	nth1(NumColumn, Row, Cell).
	
cell_of_colour(NumRow, NumColumn, Board, Colour) :-
	cell_at(NumRow, NumColumn, Board, cell(Colour, _)).
	
board_has_cell_of_colour(Board, Colour) :-
	cell_of_colour(_, _ , Board, Colour), !.
	
%%%%%%%%%%
%% MOVE %%
%%%%%%%%%%

number_of(Elem, Set, Number) :-
	delete(Set, Elem, Rest),
	length(Set, N),
	length(Rest, NR),
	Number is N - NR.

position(Row, Column) :- integer(Row), integer(Column), Row > 0, Column > 0.

next_row(black, Rowi, Rowf, Number) :- Rowf is Rowi + Number.
next_row(white, Rowi, Rowf, Number) :- Rowf is Rowi - Number.
	
value_unit_diff(ValI, ValF, 0) :- ValI =:= ValF.
value_unit_diff(ValI, ValF, 1) :- ValI < ValF.
value_unit_diff(ValI, ValF, -1) :- ValI > ValF.
	
path_from_to(position(Row, Column), position(Row, Column), [position(Row, Column)]) :- !.
	
path_from_to(position(Row, Column), position(Rowf, Columnf), Path) :-
	value_unit_diff(Row, Rowf, DiffRow),
	value_unit_diff(Column, Columnf, DiffColumn),
	NextRow is Row + DiffRow,
	NextColumn is Column + DiffColumn,
	Path = [position(Row, Column) | Tail],
	path_from_to(position(NextRow, NextColumn), position(Rowf, Columnf), Tail).
	
	
possible_moves_aux(circle, Row, Column, Row1f, Column1f, Column2f, [Path1, Path2]) :-
	path_from_to(position(Row, Column), position(Row1f, Column1f), Path1),
	path_from_to(position(Row, Column), position(Row1f, Column2f), Path2).
	
possible_moves_aux(square, Row, Column, Row1f, Column1f, Column2f, [Path1, Path2, Path3]) :-
	path_from_to(position(Row, Column), position(Row1f, Column), Path1),
	path_from_to(position(Row, Column), position(Row, Column1f), Path2),
	path_from_to(position(Row, Column), position(Row, Column2f), Path3).
	
possible_moves(_, _, _, _, _, 0, []).
	
possible_moves(position(Row, Column), NumBoardRows, NumberBoardCols, Piece, Colour, Number, Moves) :-
	next_row(Colour, Row, Row1f, Number),
	Column1f is Column + Number,
	Column2f is Column - Number,
	NewNumber is Number - 1,
	possible_moves_aux(Piece, Row, Column, Row1f, Column1f, Column2f, MyMoves),
	bounce_moves(MyMoves, NumberBoardCols, MyMovesWithBounces),
	include(move_in_board(NumBoardRows, NumberBoardCols), MyMovesWithBounces, MyMovesInBoard),
	append(MyMovesInBoard, RestOfMoves, Moves),
	possible_moves(position(Row, Column), NumBoardRows, NumberBoardCols, Piece, Colour, NewNumber, RestOfMoves).
	
write_list([]) :- !.
write_list([Head | Tail]) :- write(Head), nl, write_list(Tail).
	
bounce_path([], _, []).
	
bounce_path([position(Row, 1) | Tail], _, [position(Row, 1) | Tail]).
	
bounce_path([position(Row, Col) | Tail], NumberBoardCols, [position(Row, Col) | Tail]) :-
	Col =:= NumberBoardCols.

bounce_path([position(Row, Col) | Tail], NumberBoardCols, [position(Row, Col) | TailB]) :-
	Col >= 2,
	Col < NumberBoardCols,
	bounce_path_aux(Tail, NumberBoardCols, TailB).
	
bounce_path_aux([], _, []).
	
bounce_path_aux([position(Row, Col) | Tail], NumberBoardCols, [position(Row, ColB) | TailB]) :-
	Col > NumberBoardCols,
	ColB is 2 * NumberBoardCols - Col,
	bounce_path_aux(Tail, NumberBoardCols, TailB).
	
bounce_path_aux([position(Row, Col) | Tail], NumberBoardCols, [position(Row, ColB) | TailB]) :-
	Col < 1,
	ColB is 2 - Col,
	bounce_path_aux(Tail, NumberBoardCols, TailB).
	
bounce_path_aux([Position | Tail], NumberBoardCols, [Position | TailB]) :-
	bounce_path_aux(Tail, NumberBoardCols, TailB).
	
bounce_moves([], _, []).
	
bounce_moves([HeadMove | TailMoves], NumberBoardCols, [HeadBouncedMove | TailBouncedMoves]) :-
	bounce_path(HeadMove, NumberBoardCols, HeadBouncedMove),
	bounce_moves(TailMoves, NumberBoardCols, TailBouncedMoves).
	
move_in_board(NumBoardRows, NumberBoardCols, MovePath) :-
	last(MovePath, position(DestRow, DestCol)),
	DestRow >= 1,
	DestCol >= 1,
	DestRow =< NumBoardRows,
	DestCol =< NumberBoardCols.
	
is_valid_move(Board, Move) :-
    last(Move, position(Final_row, Final_column)), 
	cell_at(Final_row, Final_column, Board, cell(blank)), !.    

is_valid_move(Board, Move) :-
    Move = [position(Init_row, Init_column) | _ ],
    last(Move, position(Final_row, Final_column)), 
    cell_at(Init_row, Init_column, Board, cell(Colour, ElemsI)),				% Get the cell in the initial position
	cell_at(Final_row, Final_column, Board, cell(Colour, ElemsF)),				% Get the cell in the final position
    !,
	length(ElemsI, Ni),
	length(ElemsF, Nf),
	Nt is Ni + Nf,
	Nt =< 3.
    
is_valid_move(Board, Move) :-
    Move = [position(Init_row, Init_column) | _ ],
    last(Move, position(Final_row, Final_column)), 
    cell_at(Init_row, Init_column, Board, cell(Colour, _)),      % Get the cell in the initial position
    different_valid_colour(Colour, OtherColour),
	cell_at(Final_row, Final_column, Board, cell(OtherColour, _)).
    
available_moves(position(Row, Column), Board, Moves) :-
	number_of_board_rows(Board, N_Rows),
	number_of_board_columns(Board, N_Columns),
	cell_at(Row, Column, Board, cell(Colour, Elems)),
	number_of(square, Elems, Ns),
	number_of(circle, Elems, Nc),
	possible_moves(position(Row, Column), N_Rows, N_Columns, square, Colour, Ns, MovesS), !,
	possible_moves(position(Row, Column), N_Rows, N_Columns, circle, Colour, Nc, MovesC), !,
	append(MovesS, MovesC, MovesT),
	include(is_path_clear(Board), MovesT, MovesF),
    include(is_valid_move(Board), MovesF, Moves).
	
is_board_cell_blank(Board, position(Row, Column)) :- cell_at(Row, Column, Board, cell(blank)).
	
is_path_clear(Board, MovePath) :-
	append([_ | Path], [_], MovePath),
	maplist(is_board_cell_blank(Board), Path).

%%%%%%%%%%%%%%%%%%
%% UPDATE BOARD %%
%%%%%%%%%%%%%%%%%%
	
update_board([], []).
update_board([RowI | RestOfRowsI], [RowU | RestOfRowsU]) :- update_board_row(RowI, RowU), update_board(RestOfRowsI, RestOfRowsU).

update_board_row([], []) :- !.

update_board_row([Headi | Taili], [Headi | Tailu]) :-
	update_board_row(Taili, Tailu).

update_board_row([_ | Taili], [Headu | Tailu]) :-
	nonvar(Headu),
	update_board_row(Taili, Tailu).
	
%%%%%%%%%%
%% MOVE %%
%%%%%%%%%%


last_member(Elem, [Head | _]) :- last(Head, Elem).
last_member(Elem, [_ | Tail]) :- last_member(Elem, Tail).

move(Position, Position, Board, Board) :-
	available_moves(Position, Board, AvailableMoves), 	% Get all availableMoves
	last_member(Position, AvailableMoves).

move(position(Rowi, Columni), position(Rowf, Columnf), Boardi, Boardf) :-
	available_moves(position(Rowi, Columni), Boardi, AvailableMoves), 	% Get all availableMoves
	last_member(position(Rowf, Columnf), AvailableMoves), 				% Verify if the requested move is valid
	cell_at(Rowi, Columni, Boardi, cell(Colour, ElemsI)),				% Get the cell in the initial position
	cell_at(Rowf, Columnf, Boardi, cell(Colour, ElemsF)),				% Get the cell in the final position
	length(ElemsI, Ni),
	length(ElemsF, Nf),
	Nt is Ni + Nf,
	Nt =< 3,
	append(ElemsF, ElemsI, ElemsT),
	board_dimensions(Boardi, N_Rows, N_Columns),						% Get Board dimensions
	create_board(N_Rows,N_Columns,Boardf),								% Create new board with the same size
	cell_at(Rowf, Columnf, Boardf, cell(Colour, ElemsT)),								
	cell_at(Rowi, Columni, Boardf, cell(blank)),
	update_board(Boardi, Boardf).
	
move(position(Rowi, Columni), position(Rowf, Columnf), Boardi, Boardf) :-
	available_moves(position(Rowi, Columni), Boardi, AvailableMoves), 	% Get all availableMoves
	last_member(position(Rowf, Columnf), AvailableMoves), 					% Verify if the requested move is valid
	cell_at(Rowf, Columnf, Boardi, cell(blank)),						% Get the cell in the final position
	cell_at(Rowi, Columni, Boardi, Celli),								% Get the cell in the initial position
	board_dimensions(Boardi, N_Rows, N_Columns),						% Get Board dimensions
	create_board(N_Rows,N_Columns,Boardf),								% Create new board with the same size
	cell_at(Rowf, Columnf, Boardf, Celli),								
	cell_at(Rowi, Columni, Boardf, cell(blank)),
	update_board(Boardi, Boardf), !.
	
move(position(Rowi, Columni), position(Rowf, Columnf), Boardi, Boardf) :-
	available_moves(position(Rowi, Columni), Boardi, AvailableMoves), 	% Get all availableMoves
	last_member(position(Rowf, Columnf), AvailableMoves), 					% Verify if the requested move is valid
	cell_at(Rowi, Columni, Boardi, cell(Colour, ElemsI)),				% Get the cell in the initial position
	different_valid_colour(Colour, OtherColour),
	cell_at(Rowf, Columnf, Boardi, cell(OtherColour, _)),				% Get the cell in the final position
	board_dimensions(Boardi, N_Rows, N_Columns),						% Get Board dimensions
	create_board(N_Rows,N_Columns,Boardf),								% Create new board with the same size
	cell_at(Rowf, Columnf, Boardf, cell(Colour, ElemsI)),								
	cell_at(Rowi, Columni, Boardf, cell(blank)),
	update_board(Boardi, Boardf), !.
	
%%%%%%%%%%%%%%%%%%%%%%
%% USER INTERACTION %%
%%%%%%%%%%%%%%%%%%%%%%

is_valid_column_char(Char) :-
	atom_codes(Char, Codes),
	length(Codes, 1),
	is_alpha(Char).

read_position(Row, Column) :-
	write('Cell: '),
    catch(read(Cell), _, fail),
    atom_codes(Cell, [Column_Code | Row_str]),
    catch(number_codes(Row, Row_str), _, fail),
    atom_codes(Column_Char, [Column_Code]),
	is_valid_column_char(Column_Char),
	column_letter(Column, Column_Char).
	
is_valid_mode(player).
is_valid_mode(pc).
    
play(N_Rows, N_Columns, Player_1, Player_2) :-
    is_valid_mode(Player_1),
    is_valid_mode(Player_2),
    initialize_board(N_Rows, N_Columns, Board),
    play_round(Board, Player_1, Player_2, 1).
    
play_round(Board, _, _, _) :-
    \+ board_has_cell_of_colour(Board, black), !,
    write('Player 1 wins!').
    
play_round(Board, _, _, _) :-
    \+ board_has_cell_of_colour(Board, white), !,
    write('Player 2 wins!').   
    
play_round(Board, Player_1, Player_2, 1) :-
    write('Player 1 turn'), nl,
    make_move(Board, (Player_1, white), Board_after),
    play_round(Board_after, Player_1, Player_2, 2).    

play_round(Board, Player_1, Player_2, 2) :-
    write('Player 2 turn'), nl,
    make_move(Board, (Player_2, black), Board_after),
    play_round(Board_after, Player_1, Player_2, 1).        
    
    
read_valid_position(Row, Column, Colour, Board, Row_1, Column_1) :-
    integer(Row_1),
    integer(Column_1),
    cell_of_colour(Row_1, Column_1, Board, Colour), !,
    Row = Row_1, Column = Column_1.
    
read_valid_position(_, _, Colour, Board, Row_1, Column_1) :-
    integer(Row_1),
    integer(Column_1),
    \+ cell_of_colour(Row_1, Column_1, Board, Colour),
    write('You cannot play that cell.'), nl, fail.
    
read_valid_position(Row, Column, Colour, Board, _ , _) :-
    read_position(Row_1, Column_1), !,
    read_valid_position(Row, Column, Colour, Board, Row_1, Column_1).
    
read_valid_position(Row, Column, Colour, Board, _ , _) :- write('Invalid position. Please try again.'), nl, read_valid_position(Row, Column, Colour, Board, _ , _).

read_valid_position(Row, Column, Colour, Board) :- read_valid_position(Row, Column, Colour, Board, _, _).
    
print_position(position(Row, Column)) :-
    column_letter(Column, Letter),
    write(Letter), write(Row).
    
print_move(Move, Board, _) :-
    Move = [position(FirstRow, FirstColumn) | _],
    last(Move, position(LastRow, LastColumn)),
    cell_at(LastRow, LastColumn, Board, cell(blank)),
    print_position(position(FirstRow, FirstColumn)),
    write(' - '),
    print_position(position(LastRow, LastColumn)).
    
print_move(Move, Board, Colour) :-
    Move = [position(FirstRow, FirstColumn) | _],
    last(Move, position(LastRow, LastColumn)),
    cell_of_colour(LastRow, LastColumn, Board, Colour),
    print_position(position(FirstRow, FirstColumn)),
    write(' - '),
    print_position(position(LastRow, LastColumn)), write('+').
    
print_move(Move, Board, Colour) :-
    Move = [position(FirstRow, FirstColumn) | _],
    last(Move, position(LastRow, LastColumn)),
    different_valid_colour(Colour, OtherColour),
    cell_of_colour(LastRow, LastColumn, Board, OtherColour),
    print_position(position(FirstRow, FirstColumn)),
    write(' x '),
    print_position(position(LastRow, LastColumn)).
    
print_move_list([], _, _, _).
    
print_move_list([First_Move | Rest_Of_Moves], Board, Colour, N) :-
    write(N), write(') '), print_move(First_Move, Board, Colour), nl,
    N_1 is N + 1,
    print_move_list(Rest_Of_Moves, Board, Colour, N_1).

print_move_list(Moves_List, Board, Colour) :- print_move_list(Moves_List, Board, Colour, 1).

read_exec_move(Colour, Board, Board_Final, _, _, 0, _) :- !,
    read_valid_position(Row, Column, Colour, Board),
    
    available_moves(position(Row, Column), Board, AvailableMoves),
    nl, write('Possible moves: '), nl,
    write('0) Choose another cell.'), nl,
    print_move_list(AvailableMoves, Board, Colour),
    
    nl, write('Choose a move: '), read(Num_Move),
    read_exec_move(Colour, Board, Board_Final, Row, Column, Num_Move, AvailableMoves).
    
read_exec_move(_, Board, Board_Final, Row, Column, Num_Move, AvailableMoves) :-
    integer(Num_Move),
    nth1(Num_Move, AvailableMoves, Move), !,
    last(Move, Dest_Position),
    move(position(Row, Column), Dest_Position, Board, Board_Final).
    
read_exec_move(Colour, Board, Board_Final, Row, Column, _, AvailableMoves) :- write('Invalid Move. Choose a move: '), read(Num_Move), read_exec_move(Colour, Board, Board_Final, Row, Column, Num_Move, AvailableMoves).
    
make_move(Board, (player, Colour), Board_Final) :-
    print_board(Board),
    read_exec_move(Colour, Board, Board_Final, _, _, 0, _).
    
    
    
    
    
    
% read_position(